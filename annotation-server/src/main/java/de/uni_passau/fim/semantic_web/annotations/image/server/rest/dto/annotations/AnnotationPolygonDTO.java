/*
 * ==========================License-Start=============================
 * annotation-server : AnnotationPolygonDTO
 *
 * Copyright © 2018 Universität Passau, Germany
 *
 * MIT License
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * ==========================License-End==============================
 */

package de.uni_passau.fim.semantic_web.annotations.image.server.rest.dto.annotations;

import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.annotation.Annotation;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.annotation.shapes.AnnotationPolygon;

import java.util.List;

public class AnnotationPolygonDTO extends AnnotationDTO<AnnotationPolygonDTO> {
    private List<AnnotationPolygon.PolygonPoint> points;

    public List<AnnotationPolygon.PolygonPoint> getPoints() {
        return points;
    }

    public void setPoints(List<AnnotationPolygon.PolygonPoint> points) {
        this.points = points;
    }

    @Override
    public Annotation toConcrete() {
        return mapper().map(this, AnnotationPolygon.Builder.class).build();
    }

    @Override
    public AnnotationPolygonDTO fromConcrete(Annotation concrete) {
        mapper().map(concrete, this);
        return this;
    }
}
