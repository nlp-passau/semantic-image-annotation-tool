/*
 * ==========================License-Start=============================
 * annotation-server : ServerStatsBean
 *
 * Copyright © 2018 Universität Passau, Germany
 *
 * MIT License
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * ==========================License-End==============================
 */

package de.uni_passau.fim.semantic_web.annotations.image.server.rest.beans;

/**
 * The {@link ServerStatsBean} class is used to store status information about the server.
 */
public class ServerStatsBean {
    private final int availableCPUs;
    private final long freeMemory;
    private final long allocatedMemory;
    private final long maxMemory;
    private final double load;
    private final String osName;
    private final String osVersion;
    private final String arch;

    /**
     * Creates a new, empty instance.
     */
    private ServerStatsBean(ServerStatsBean.Builder builder) {
        this.availableCPUs = builder.availableCPUs;
        this.freeMemory = builder.freeMemory;
        this.allocatedMemory = builder.allocatedMemory;
        this.maxMemory = builder.maxMemory;
        this.load = builder.load;
        this.osName = builder.osName;
        this.osVersion = builder.osVersion;
        this.arch = builder.arch;
    }

    /**
     * Gets the {@link #availableCPUs} field.
     *
     * @return Value inside the {@link #availableCPUs} field.
     */
    public int getAvailableCPUs() {
        return availableCPUs;
    }

    /**
     * Gets the {@link #freeMemory} field.
     *
     * @return Value inside the {@link #freeMemory} field.
     */
    public long getFreeMemory() {
        return freeMemory;
    }

    /**
     * Gets the {@link #maxMemory} field.
     *
     * @return Value inside the {@link #maxMemory} field.
     */
    public long getMaxMemory() {
        return maxMemory;
    }

    /**
     * Gets the {@link #allocatedMemory} field.
     *
     * @return Value inside the {@link #allocatedMemory} field.
     */
    public long getAllocatedMemory() {
        return allocatedMemory;
    }

    /**
     * Gets the {@link #load} field.
     *
     * @return Value inside the {@link #load} field.
     */
    public double getLoad() {
        return load;
    }


    /**
     * Gets the {@link #osName} field.
     *
     * @return Value inside the {@link #osName} field.
     */
    public String getOsName() {
        return osName;
    }

    /**
     * Gets the {@link #osVersion} field.
     *
     * @return Value inside the {@link #osVersion} field.
     */
    public String getOsVersion() {
        return osVersion;
    }

    /**
     * Gets the {@link #arch} field.
     *
     * @return Value inside the {@link #arch} field.
     */
    public String getArch() {
        return arch;
    }


    public final static class Builder {
        Integer availableCPUs;
        Long freeMemory;
        Long allocatedMemory;
        Long maxMemory;
        Double load;
        String osName;
        String osVersion;
        String arch;

        public ServerStatsBean.Builder availableCPUs(final int availableCPUs) {
            this.availableCPUs = availableCPUs;
            return this;
        }

        public ServerStatsBean.Builder freeMemory(final long freeMemory) {
            this.freeMemory = freeMemory;
            return this;
        }

        public ServerStatsBean.Builder allocatedMemory(final long allocatedMemory) {
            this.allocatedMemory = allocatedMemory;
            return this;
        }

        public ServerStatsBean.Builder maxMemory(final long maxMemory) {
            this.maxMemory = maxMemory;
            return this;
        }

        public ServerStatsBean.Builder load(final double load) {
            this.load = load;
            return this;
        }

        public ServerStatsBean.Builder osName(final String osName) {
            this.osName = osName;
            return this;
        }

        public ServerStatsBean.Builder osVersion(final String osVersion) {
            this.osVersion = osVersion;
            return this;
        }

        public ServerStatsBean.Builder arch(final String arch) {
            this.arch = arch;
            return this;
        }

        public ServerStatsBean build() {
            return new ServerStatsBean(this);
        }
    }
}
