/*
 * ==========================License-Start=============================
 * annotation-server : AnnotationCircle
 *
 * Copyright © 2018 Universität Passau, Germany
 *
 * MIT License
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * ==========================License-End==============================
 */

package de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.annotation.shapes;

import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.annotation.Annotation;

/**
 * This class defines a circle-shaped {@link Annotation}.
 */
public class AnnotationCircle extends Annotation {

    private int radius;

    /**
     * Creates a new annotation with the ontology class and ontology context set.
     *
     * @param builder Ontology class this annotation refers to.
     */
    protected AnnotationCircle(final AnnotationCircle.Builder builder) {
        super(builder);
        this.radius = builder.radius;
    }

    /**
     * Gets the radius of this {@link AnnotationCircle}.
     *
     * @return Radius of this {@link AnnotationCircle}.
     */
    public int getRadius() {
        return radius;
    }

    public static class Builder extends Annotation.Builder {

        private int radius;

        public AnnotationCircle.Builder radius(final int radius) {
            this.radius = radius;
            return this;
        }

        @Override
        public AnnotationCircle build() {
            return new AnnotationCircle(this);
        }
    }
}
