package de.uni_passau.fim.semantic_web.annotations.image.server.core.exceptions;

public class UnsupportedImageFormatException extends RuntimeException {
    public UnsupportedImageFormatException() {
        super();
    }

    public UnsupportedImageFormatException(String message) {
        super(message);
    }
}
