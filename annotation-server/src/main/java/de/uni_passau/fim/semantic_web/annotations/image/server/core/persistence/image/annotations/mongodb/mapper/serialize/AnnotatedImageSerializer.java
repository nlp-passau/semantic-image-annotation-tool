package de.uni_passau.fim.semantic_web.annotations.image.server.core.persistence.image.annotations.mongodb.mapper.serialize;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.AnnotationCore;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.image.AnnotatedImage;

import java.io.IOException;

public class AnnotatedImageSerializer extends ImageSerializer<AnnotatedImage> {
    public AnnotatedImageSerializer(AnnotationCore core) {
        super(core);
    }

    @Override
    public void serialize(AnnotatedImage image, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        super.serialize(image, jsonGenerator, serializerProvider);

        jsonGenerator.writeArrayFieldStart("annotations");
        image.getAnnotations()
            .forEach(annotation -> {
                try {
                    getMapper().writeValue(jsonGenerator, annotation);
                } catch (IOException e) {
                    log.error(MARKER, "Could not write annotation to json.", e);
                }
        });
        jsonGenerator.writeEndArray();
    }
}
