/*
 * ==========================License-Start=============================
 * annotation-server : MongoAnnotationsStorage
 *
 * Copyright © 2018 Universität Passau, Germany
 *
 * MIT License
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * ==========================License-End==============================
 */

package de.uni_passau.fim.semantic_web.annotations.image.server.core.persistence.image.annotations.mongodb;

import com.fasterxml.jackson.databind.MapperFeature;
import com.mongodb.DB;
import com.mongodb.MongoClient;
import com.typesafe.config.Config;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.AnnotationCore;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.image.AnnotatedImage;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.image.Image;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.annotation.Annotation;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.persistence.image.annotations.AnnotationsStorage;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.persistence.image.annotations.mongodb.mapper.AnnotationMongoModule;
import org.bson.types.ObjectId;
import org.jongo.Find;
import org.jongo.Jongo;
import org.jongo.MongoCollection;
import org.jongo.MongoCursor;
import org.jongo.marshall.jackson.JacksonMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class MongoAnnotationsStorage extends AnnotationsStorage<MongoId> {

    private final Logger log = LoggerFactory.getLogger(MongoAnnotationsStorage.class);

    private final MongoCollection annotations;

    public MongoAnnotationsStorage(AnnotationCore core) {
        super(core);

        final Config db_config = core.getConfig().getConfig("mongodb");

        log.debug(MARKER, "Initializing Mongo connection.");
        MongoClient connection = new MongoClient(db_config.getString("host"), db_config.getInt("port"));
        @SuppressWarnings("deprecation") DB db = connection.getDB(db_config.getString("db.name"));

        log.debug(MARKER, "Initializing Jongo");
        Jongo jongo = new Jongo(db, new JacksonMapper.Builder()
            .registerModule(new AnnotationMongoModule(core))
            .enable(MapperFeature.AUTO_DETECT_GETTERS)
            .build());
        this.annotations = jongo.getCollection("annotations");
    }

    @Override
    public Stream<Image> getImageList() {
        return StreamSupport.stream(annotations.find().as(Image.class).spliterator(), false);
    }

    @Override
    public AnnotatedImage getImage(MongoId imageId) {
        log.debug(MARKER, "Retrieve image from database with id '{}'", imageId.toString());
        final Optional<AnnotatedImage> image = Optional
            .ofNullable(annotations.findOne(imageId.getId()).as(AnnotatedImage.class));

        if (image.isPresent()) {
            log.info(MARKER, "Found image with id '{}' in database.", imageId);
            return image.get();
        } else {
            log.warn("Image with id '{}' is not in database.", imageId);
            return null;
        }
    }

    @Override
    public Image storeImage(AnnotatedImage img) {
        log.debug(MARKER, "Will store {} annotations for image with id '{}' in database",
            img.getAnnotations().size(), img.getId());

        annotations.insert(img).getUpsertedId();

        log.info(MARKER, "Successfully stored {} annotations for image '{}' in database.",
            img.getAnnotations().size(), img.getId());
        return img;
    }

    @Override
    public synchronized void updateImage(Image target, List<Annotation> newAnnotations) {
        if (annotations == null) {
            throw new IllegalArgumentException("New annotations must not be null!");
        }

        if (target == null) {
            throw new IllegalArgumentException("No target image specified!");
        }

        log.info(MARKER, "Updating annotations of image {} with new annotation list of size {}",
            target.getId().toString(), newAnnotations.size());

        final AnnotatedImage newImage = new AnnotatedImage.Builder<>()
            .annotations(newAnnotations)
            .filename(target.getFilename())
            .size(target.getSize())
            .id(target.getId())
            .build();

        annotations.save(newImage);

        log.info(MARKER, "Successfully replaced the annotations for image with id '{}' with '{}' new annotations.",
            target.getId(), newAnnotations.size());
    }

    @Override
    public synchronized void deleteImage(Image img) {
        log.debug(MARKER, "Will remove image with id '{}' from database.", img.getId());
        annotations.remove((ObjectId) img.getId().getId());
        log.info(MARKER, "Image with id '{}' successfully removed from database.", img.getId());
    }
}
