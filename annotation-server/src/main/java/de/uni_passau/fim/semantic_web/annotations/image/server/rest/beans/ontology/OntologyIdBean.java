/*
 * ==========================License-Start=============================
 * annotation-server : OntologyIdBean
 *
 * Copyright © 2018 Universität Passau, Germany
 *
 * MIT License
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * ==========================License-End==============================
 */

package de.uni_passau.fim.semantic_web.annotations.image.server.rest.beans.ontology;

import com.fasterxml.jackson.annotation.JsonProperty;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.Id;
import de.uni_passau.fim.semantic_web.annotations.image.server.rest.beans.annotations.ImageUploadResponseBean;

/**
 * Bean containing the id of an
 * {@link de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.ontology.Ontology}
 * as well as its name.
 */
public class OntologyIdBean {
    private Id id;
    private String name;

    /**
     * Creates a new instance with no fields set.
     */
    public OntologyIdBean() {
    }

    /**
     * Creates a new instance.
     *
     * @param id ID of the corresponding ontology.
     * @param name Name of the corresponding ontology.
     */
    public OntologyIdBean(Id id, String name) {
        this.id = id;
        this.name = name;
    }

    /**
     * Gets the image id as string
     *
     * @return {@link String} containing the image id this {@link ImageUploadResponseBean} belongs
     * to.
     */
    @JsonProperty("id")
    public String getImageId() {
        return id.toString();
    }

    /**
     * Gets the ontology name.
     *
     * @return Ontology name set in this bean.
     */
    public String getName() {
        return name;
    }
}
