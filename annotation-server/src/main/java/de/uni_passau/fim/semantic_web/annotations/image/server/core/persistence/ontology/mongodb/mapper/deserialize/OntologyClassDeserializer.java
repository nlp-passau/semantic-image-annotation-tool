/*
 * ==========================License-Start=============================
 * annotation-server : OntologyClassDeserializer
 *
 * Copyright © 2018 Universität Passau, Germany
 *
 * MIT License
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * ==========================License-End==============================
 */

package de.uni_passau.fim.semantic_web.annotations.image.server.core.persistence.ontology.mongodb.mapper.deserialize;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.ontology.OntologyClass;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.persistence.image.annotations.mongodb.MongoId;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.persistence.ontology.mongodb.mapper.OntologyMongoModule;
import org.bson.types.ObjectId;

import java.io.IOException;
import java.util.SortedMap;
import java.util.TreeSet;

public class OntologyClassDeserializer extends JsonDeserializer<OntologyClass> {

    @Override
    public OntologyClass deserialize(JsonParser jsonParser, DeserializationContext deserializationContext)
            throws IOException {

        final OntologyClass.Builder builder = new OntologyClass.Builder();

        final ObjectMapper mapper = new ObjectMapper();
        final ObjectCodec codec = jsonParser.getCodec();
        final JsonNode root = codec.readTree(jsonParser);

        builder.id(new MongoId(mapper.treeToValue(root.get("_id"), ObjectId.class)));
        builder.className(root.get("className").asText());
        builder.classIri(root.get("classIri").asText());

        // Fetch literals and comments.
        SortedMap<String, String> literals = mapper.readValue(root.get("literals").toString(),
            new TypeReference<SortedMap<String, String>>() {
            });
        SortedMap<String, String> comments = mapper.readValue(root.get("comments").toString(),
            new TypeReference<SortedMap<String, String>>() {
            });

        builder.literals(literals);
        builder.comments(comments);

        // Instantiate lists for subclasses. Note that setting the subclass references
        // is done through the Ontology serializer itself.
        builder.subClasses(new TreeSet<>());

        return builder.build();
    }
}
