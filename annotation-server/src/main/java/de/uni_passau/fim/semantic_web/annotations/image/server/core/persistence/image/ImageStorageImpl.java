/*
 * ==========================License-Start=============================
 * annotation-server : ImageStorageImpl
 *
 * Copyright © 2018 Universität Passau, Germany
 *
 * MIT License
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * ==========================License-End==============================
 */

package de.uni_passau.fim.semantic_web.annotations.image.server.core.persistence.image;

import de.uni_passau.fim.semantic_web.annotations.image.server.core.AnnotationCore;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.AnnotationCoreServiceFeature;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.image.AnnotatedImage;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.image.Image;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.annotation.Annotation;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.persistence.image.annotations.AnnotationsStorage;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.persistence.image.annotations.mongodb.MongoAnnotationsStorage;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.persistence.image.annotations.mongodb.MongoId;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.persistence.image.imgdata.FileImageDataStorage;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.persistence.image.imgdata.ImageDataStorage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

public class ImageStorageImpl extends ImageStorage<MongoId> {
    private final static Marker MARKER = MarkerFactory.getMarker(AnnotationCoreServiceFeature.MARKER.getName()
        + "_imageStorage");

    private final Logger log = LoggerFactory.getLogger(ImageStorageImpl.class);

    private AnnotationsStorage<MongoId> annotationsStorage;
    private ImageDataStorage imageStorage;

    public ImageStorageImpl(AnnotationCore core) {
        super(core);
        this.annotationsStorage = new MongoAnnotationsStorage(core);
        this.imageStorage = new FileImageDataStorage(core);
    }

    @Override
    public Stream<Image> getImages() {
        return annotationsStorage.getImageList();
    }

    @Override
    public AnnotatedImage getImageAnnotations(MongoId id) {
        return annotationsStorage.getImage(id);
    }

    @Override
    public byte[] getImage(MongoId id) {
        final Image target = Optional.ofNullable(annotationsStorage.getImage(id)).orElse(null);
        return imageStorage.getImage(target);
    }

    @Override
    public byte[] getImageThumbnail(MongoId id) {
        return imageStorage.getThumbnail(annotationsStorage.getImage(id));
    }

    @Override
    public void updateImage(MongoId id, byte[] data) {
        Image target = annotationsStorage.getImage(id);
        imageStorage.updateImage(target, data);
    }

    @Override
    public void updateImage(MongoId id, List<Annotation> annotations) {
        Image target = annotationsStorage.getImage(id);
        annotationsStorage.updateImage(target, annotations);
    }

    @Override
    public void deleteImage(MongoId id) {
        Image target = annotationsStorage.getImage(id);
        annotationsStorage.deleteImage(target);
        imageStorage.deleteImage(target);
    }

    @Override
    public AnnotatedImage uploadImage(byte[] imgData, List<Annotation> annotations, String filename) {
        Image.Size size;

        if (annotations == null) {
            annotations = new LinkedList<>();
        }

        try (final ByteArrayInputStream bis = new ByteArrayInputStream(imgData)) {
            final BufferedImage img = ImageIO.read(ImageIO.createImageInputStream(bis));
            bis.close();

            size = new Image.Size(img.getWidth(), img.getHeight());

        } catch (IOException e) {
            log.error(MARKER, "Could not read image from file.", e);
            return null;
        }

        final AnnotatedImage newImage = new AnnotatedImage.Builder<>()
            .filename(filename)
            .size(size)
            .annotations(annotations)
            .id(this.core.getIdFactory().createId())
            .build();

        annotationsStorage.storeImage(newImage);
        imageStorage.storeImage(newImage, imgData);
        return newImage;
    }
}
