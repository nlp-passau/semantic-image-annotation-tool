/*
 * ==========================License-Start=============================
 * annotation-server : AnnotationRectangle
 *
 * Copyright © 2018 Universität Passau, Germany
 *
 * MIT License
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * ==========================License-End==============================
 */

package de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.annotation.shapes;

import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.annotation.Annotation;

/**
 * This class defines a rectangle-shaped {@link Annotation}.
 */
public class AnnotationRectangle extends Annotation {

    private int top;
    private int left;
    private int width;
    private int height;
    private double scaleX;
    private double scaleY;

    private AnnotationRectangle(final AnnotationRectangle.Builder builder) {
        super(builder);
        this.top = builder.top;
        this.left = builder.left;
        this.width = builder.width;
        this.height = builder.height;
        this.scaleX = builder.scaleX;
        this.scaleY = builder.scaleY;
    }

    public int getTop() {
        return top;
    }

    public int getLeft() {
        return left;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public double getScaleX() {
        return scaleX;
    }

    public double getScaleY() {
        return scaleY;
    }

    public static class Builder extends Annotation.Builder {
        private int top;
        private int left;
        private int width;
        private int height;
        private double scaleX;
        private double scaleY;

        public AnnotationRectangle.Builder top(final int top) {
            this.top = top;
            return this;
        }

        public AnnotationRectangle.Builder left(final int left) {
            this.left = left;
            return this;
        }

        public AnnotationRectangle.Builder width(final int width) {
            this.width = width;
            return this;
        }

        public AnnotationRectangle.Builder height(final int height) {
            this.height = height;
            return this;
        }

        public AnnotationRectangle.Builder scaleX(final double scaleX) {
            this.scaleX = scaleX;
            return this;
        }

        public AnnotationRectangle.Builder scaleY(final double scaleY) {
            this.scaleY = scaleY;
            return this;
        }

        @Override
        public AnnotationRectangle build() {
            return new AnnotationRectangle(this);
        }
    }
}
