/*
 * ==========================License-Start=============================
 * annotation-server : AnnotationCore
 *
 * Copyright © 2018 Universität Passau, Germany
 *
 * MIT License
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * ==========================License-End==============================
 */

package de.uni_passau.fim.semantic_web.annotations.image.server.core;

import com.typesafe.config.Config;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.automatic_annotation.AutomaticAnnotationService;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.bgtask.BackgroundTaskManager;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.Id;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.IdFactory;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.importer.ImportManager;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.persistence.image.ImageStorage;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.persistence.ontology.OntologyStorage;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

/**
 * This interface defines all functionality that an annotation-core service implementation must
 * offer.
 * <p>
 * Classes implementing this interface must provide an {@link ImageStorage} as well as an
 * {@link OntologyStorage}.
 */
public abstract class AnnotationCore {

    protected final static Marker MARKER = MarkerFactory.getMarker("core");

    private final Config config;

    protected AnnotationCore(Config config) {
        this.config = config;
    }

    public Config getConfig() {
        return this.config;
    }

    public abstract ImageStorage<Id> getImageStorage();

    public abstract OntologyStorage<Id> getOntologyStorage();

    public abstract AutomaticAnnotationService getAutomaticAnnotationService();

    public abstract ImportManager getImportManager();

    public abstract BackgroundTaskManager getBackgroundTaskManager();

    public abstract IdFactory getIdFactory();
}
