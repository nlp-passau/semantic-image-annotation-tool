/*
 * ==========================License-Start=============================
 * annotation-server : AnnotationSerializer
 *
 * Copyright © 2018 Universität Passau, Germany
 *
 * MIT License
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * ==========================License-End==============================
 */

package de.uni_passau.fim.semantic_web.annotations.image.server.core.persistence.image.annotations.mongodb.mapper.serialize;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.jsontype.TypeSerializer;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.AnnotationCore;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.Id;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.annotation.Annotation;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.persistence.image.annotations.mongodb.mapper.deserialize.AnnotationPolygonDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.io.IOException;

public abstract class AnnotationSerializer<T extends Annotation> extends JsonSerializer<T> {

    protected final static Marker MARKER = MarkerFactory.getMarker("json");
    private final Logger log = LoggerFactory.getLogger(AnnotationPolygonDeserializer.class);

    private final AnnotationCore core;

    AnnotationSerializer(AnnotationCore core) {
        this.core = core;
    }

    @Override
    public void serialize(T annotation, JsonGenerator jsonGenerator, SerializerProvider serializerProvider)
        throws IOException {

        final Id id = annotation.getId() != null ? annotation.getId() : core.getIdFactory().createId();

        log.debug(MARKER, "Serializing: BaseAnnotation");

        jsonGenerator.writeObjectField("_id", id.getId());
        jsonGenerator.writeStringField("fill", annotation.getFill());

        // concrete annotation object
        if (annotation.getOntologyClass() != null) {
            jsonGenerator.writeObjectField("ontologyClass", annotation.getOntologyClass().getId().getId());
        } else {
            jsonGenerator.writeObjectField("ontologyClass", null);
        }

        if (annotation.getOntology() != null) {
            jsonGenerator.writeObjectField("ontologyContext", annotation.getOntology().getId().getId());
        } else {
            jsonGenerator.writeObjectField("ontologyContext", null);
        }
    }

    @Override
    public void serializeWithType(T value, JsonGenerator gen, SerializerProvider serializers, TypeSerializer typeSer) throws IOException {
        log.debug(MARKER, "Serializing with type: BaseAnnotation");
        typeSer.writeTypePrefixForObject(value, gen);
        this.serialize(value, gen, serializers);
        //typeSer.writeTypeSuffixForObject(value, gen);
    }

    public abstract void addSpecifics(T annotation, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException;
}
