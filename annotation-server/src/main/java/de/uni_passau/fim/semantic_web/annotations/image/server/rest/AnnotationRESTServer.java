/*
 * ==========================License-Start=============================
 * annotation-server : AnnotationRESTServer
 *
 * Copyright © 2018 Universität Passau, Germany
 *
 * MIT License
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * ==========================License-End==============================
 */

package de.uni_passau.fim.semantic_web.annotations.image.server.rest;

import com.typesafe.config.ConfigRenderOptions;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.AnnotationCore;
import de.uni_passau.fim.semantic_web.annotations.image.server.rest.dto.DTOMapper;
import de.uni_passau.fim.semantic_web.annotations.image.server.rest.filter.CORSFilter;
import de.uni_passau.fim.semantic_web.annotations.image.server.rest.resources.*;
import org.eclipse.jetty.server.Server;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.jetty.JettyHttpContainerFactory;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.ServerProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.net.URI;

/**
 * Main class for the REST-server component of this application. Incoming REST requests
 * are generally forwarded to the core backend
 * ({@link de.uni_passau.fim.semantic_web.annotations.image.server.core.AnnotationCore}).
 */
public class AnnotationRESTServer {
    private static final Marker MARKER = MarkerFactory.getMarker("Server");

    private final Logger log = LoggerFactory.getLogger(getClass());
    private final Server server;

    private final DTOMapper dtoMapper;

    /**
     * Creates a new REST server instance. Binds to the port specified in the supplied
     * configuration object.
     *
     * @param core {@link AnnotationCore} the wrapped AnnotationCore instance
     */
    public AnnotationRESTServer(final AnnotationCore core) {
        log.info(MARKER, "Server will be started with the following configuration values:");
        log.info(MARKER, core.getConfig().root().render(ConfigRenderOptions.concise().setFormatted(true)));

        ResourceConfig rc = new ResourceConfig();
        rc.property(ServerProperties.FEATURE_AUTO_DISCOVERY_DISABLE, true);
        rc.property(ServerProperties.BV_SEND_ERROR_IN_RESPONSE, true);
        rc.register(JacksonFeature.class);
        rc.register(MultiPartFeature.class);
        rc.register(CORSFilter.class);

        // Register AnnotationREST resources.
        AnnotationResourceFactory factory = new AnnotationResourceFactory(core);
        log.warn("The following warnings from org.glassfish.jersey.internal.inject.Providers should be ignored.");
        rc.register(factory.createResource(OntologyResource.class));
        rc.register(factory.createResource(ImageResource.class));
        rc.register(factory.createResource(ServerInfoResource.class));
        rc.register(factory.createResource(ImportResource.class));
        rc.register(factory.createResource(AutomaticAnnotationResource.class));
        rc.register(factory.createResource(BackgroundTaskResource.class));

        // Register exception mappers
        rc.packages("de.uni_passau.fim.semantic_web.annotations.image.server.rest.exceptions");

        server = JettyHttpContainerFactory.createServer(
            URI.create(core.getConfig().getString("server.uri")),
            rc,
            false);

        // Instantiate server features.
        dtoMapper = new DTOMapper(core, this);

        log.info(MARKER, "Server successfully initialized, waiting for start.");
    }

    /**
     * Starts the server.
     *
     * @throws Exception Thrown if the server could not be started.
     */
    public void start() throws Exception {
        server.start();
    }

    /**
     * Stops the server.
     */
    public void stop() {
        if (server != null) {
            log.info(MARKER, "Trying to shutdown the server...");
            try {
                server.stop();
                log.info(MARKER, "Server was shut down.");
            } catch (Exception e) {
                log.error(MARKER, "There was an error during shutting down the server: {}", e.getMessage());
            }
        } else {
            log.error(MARKER, "Server should be stopped, but wasn't initialized yet.");
        }
    }
}
