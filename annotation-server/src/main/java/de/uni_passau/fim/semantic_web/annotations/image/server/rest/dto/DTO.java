package de.uni_passau.fim.semantic_web.annotations.image.server.rest.dto;

import org.modelmapper.ModelMapper;

// A = actual, D = dto
public abstract class DTO<A, D> {
    protected static DTOMapper mapper;

    protected ModelMapper mapper() {
        return mapper.mapper();
    }

    public abstract A toConcrete();
    public abstract D fromConcrete(A concrete);
}
