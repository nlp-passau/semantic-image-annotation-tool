/*
 * ==========================License-Start=============================
 * annotation-server : ImportResource
 *
 * Copyright © 2018 Universität Passau, Germany
 *
 * MIT License
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * ==========================License-End==============================
 */

package de.uni_passau.fim.semantic_web.annotations.image.server.rest.resources;

import de.uni_passau.fim.semantic_web.annotations.image.server.core.AnnotationCore;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.Id;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.ontology.Ontology;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.importer.Importer;
import de.uni_passau.fim.semantic_web.annotations.image.server.rest.dto.importer.ImporterDTO;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.awt.List;
import java.io.File;
import java.io.IOException;
import java.util.stream.Collectors;
import java.util.zip.ZipFile;

@Path("/import")
public class ImportResource extends AbstractResource {
    public ImportResource(AnnotationCore core) {
        super(core);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAvailableImporters() {
        log.debug("Client requested list of available importers.");
        return Response.ok(
            this.core.getImportManager().getAvailableImporters()
            .parallelStream()
            .map(importer -> new ImporterDTO().fromConcrete(importer))
            .collect(Collectors.toList()))
            .build();
    }

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    public Response uploadZip(@FormDataParam("file") FormDataBodyPart file,
                              @FormDataParam("file") FormDataContentDisposition fileDetail,
                              @FormDataParam("ontologyId") String ontologyTargetId,
                              @FormDataParam("selectedImporter") String importer) {
        try {
            log.debug("Accepting ZIP file from client for import using importer {}.", importer);

            final Id id = this.core.getIdFactory().createId(ontologyTargetId);

            final File zipFile = file.getValueAs(File.class);
            final Importer selected = this.core.getImportManager().getImporterByName(importer);
            final Ontology targetOntology = this.core.getOntologyStorage().getOntology(id);

            this.core.getImportManager().importAnnotations(selected, new ZipFile(zipFile), targetOntology);

            return Response
                .status(Response.Status.CREATED)
                .build();
        } catch (IOException e) {
            log.error(MARKER, "User uploaded ontology, but server cannot handle it: {}", e.getMessage());
            log.info(MARKER, "The exception: ", e);

            return Response
                .status(Response.Status.INTERNAL_SERVER_ERROR)
                .entity(e)
                .build();
        }
    }
}
