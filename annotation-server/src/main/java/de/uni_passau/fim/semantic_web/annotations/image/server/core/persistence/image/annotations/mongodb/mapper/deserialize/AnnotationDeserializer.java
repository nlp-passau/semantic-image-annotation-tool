/*
 * ==========================License-Start=============================
 * annotation-server : AnnotationDeserializer
 *
 * Copyright © 2018 Universität Passau, Germany
 *
 * MIT License
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * ==========================License-End==============================
 */

package de.uni_passau.fim.semantic_web.annotations.image.server.core.persistence.image.annotations.mongodb.mapper.deserialize;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.AnnotationCore;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.annotation.Annotation;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.ontology.Ontology;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.ontology.OntologyClass;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.persistence.image.annotations.mongodb.MongoId;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.io.IOException;
import java.util.Optional;

/**
 *
 */
public abstract class AnnotationDeserializer<T extends Annotation> extends JsonDeserializer<T> {
    protected final static Marker MARKER = MarkerFactory.getMarker("json");
    final static ObjectMapper MAPPER = new ObjectMapper();
    protected final AnnotationCore core;
    private final Logger log = LoggerFactory.getLogger(AnnotationDeserializer.class);

    AnnotationDeserializer(AnnotationCore core) {
        this.core = core;
    }

    void deserializeBaseAnnotation(Annotation.Builder builder, JsonNode root) throws JsonProcessingException {
        // Map basic annotation data.
        builder.id(new MongoId(MAPPER.treeToValue(root.get("_id"), ObjectId.class)));

        // Ontology mapping.
        final Optional<Ontology> ontologyContext = Optional.ofNullable(
            this.core
                .getOntologyStorage()
                .getOntology(new MongoId(MAPPER.treeToValue(root.get("ontologyContext"), ObjectId.class))));

        final Optional<OntologyClass> ontologyClass = ontologyContext
            .flatMap(o -> {
                try {
                    return o.findClassById(new MongoId(MAPPER.treeToValue(root.get("ontologyClass"), ObjectId.class)));
                } catch (JsonProcessingException e) {
                    log.error(MARKER, "Could not convert mongo id to our internal mongo id.");
                }
                return Optional.empty();
            });

        builder.ontology(ontologyContext.orElse(null));
        builder.ontologyClass(ontologyClass.orElse(null));
        builder.fill(root.get("fill").asText());
    }

    @Override
    public abstract T deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException;
}
