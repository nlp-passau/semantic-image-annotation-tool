/*
 * ==========================License-Start=============================
 * annotation-server : AnnotationRectangleSerializer
 *
 * Copyright © 2018 Universität Passau, Germany
 *
 * MIT License
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * ==========================License-End==============================
 */

package de.uni_passau.fim.semantic_web.annotations.image.server.core.persistence.image.annotations.mongodb.mapper.serialize;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.jsontype.TypeSerializer;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.AnnotationCore;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.annotation.shapes.AnnotationRectangle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class AnnotationRectangleSerializer extends AnnotationSerializer<AnnotationRectangle> {

    private final Logger log = LoggerFactory.getLogger(AnnotationRectangleSerializer.class);

    public AnnotationRectangleSerializer(AnnotationCore core) {
        super(core);
    }

    @Override
    public void addSpecifics(AnnotationRectangle annotation, JsonGenerator jsonGenerator, SerializerProvider serializerProvider)
        throws IOException {
        log.debug(MARKER, "Serializing: AnnotationRectangle");
        jsonGenerator.writeNumberField("top", annotation.getTop());
        jsonGenerator.writeNumberField("left", annotation.getLeft());
        jsonGenerator.writeNumberField("width", annotation.getWidth());
        jsonGenerator.writeNumberField("height", annotation.getHeight());
        jsonGenerator.writeNumberField("scaleX", annotation.getScaleX());
        jsonGenerator.writeNumberField("scaleY", annotation.getScaleY());
    }

    @Override
    public void serializeWithType(AnnotationRectangle value, JsonGenerator gen, SerializerProvider serializers, TypeSerializer typeSer) throws IOException {
        log.debug(MARKER, "Serializing with type: AnnotationRectangle");
        super.serializeWithType(value, gen, serializers, typeSer);
        addSpecifics(value, gen, serializers);
        typeSer.writeTypeSuffixForObject(value, gen);
    }
}
