/*
 * ==========================License-Start=============================
 * annotation-server : ServerInfoResource
 *
 * Copyright © 2018 Universität Passau, Germany
 *
 * MIT License
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * ==========================License-End==============================
 */

package de.uni_passau.fim.semantic_web.annotations.image.server.rest.resources;

import de.uni_passau.fim.semantic_web.annotations.image.server.core.AnnotationCore;
import de.uni_passau.fim.semantic_web.annotations.image.server.rest.beans.ServerStatsBean;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.lang.management.ManagementFactory;
import java.lang.management.OperatingSystemMXBean;

/**
 * Resource supplying information about the current server status.
 */
@Path("admin")
public class ServerInfoResource extends AbstractResource {
    public ServerInfoResource(AnnotationCore core) {
        super(core);
    }

    /**
     * This method gathers basic information about the status of the machine the application
     * is running on. This includes:
     *
     * <ul>
     * <li>Number of CPU cores</li>
     * <li>Memory usage of the JVM</li>
     * <li>Operating System</li>
     * <li>Current System Load</li>
     * </ul>
     *
     * @return {@link Response} containing basic server information.
     */
    @GET
    @Path("/info")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getServerStats() {
        Runtime jvmStats = Runtime.getRuntime();
        OperatingSystemMXBean osInfo = ManagementFactory.getOperatingSystemMXBean();

        ServerStatsBean statsBean = new ServerStatsBean.Builder()
            .availableCPUs(jvmStats.availableProcessors())
            .freeMemory(jvmStats.freeMemory())
            .allocatedMemory(jvmStats.totalMemory())
            .maxMemory(jvmStats.maxMemory())
            .load(osInfo.getSystemLoadAverage())
            .arch(osInfo.getArch())
            .osName(osInfo.getName())
            .osVersion(osInfo.getVersion())
            .build();

        return Response.ok().entity(statsBean).build();
    }
}
