/*
 * ==========================License-Start=============================
 * annotation-server : OntologySerializer
 *
 * Copyright © 2018 Universität Passau, Germany
 *
 * MIT License
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * ==========================License-End==============================
 */

package de.uni_passau.fim.semantic_web.annotations.image.server.core.persistence.ontology.mongodb.mapper.serialize;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.ontology.Ontology;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.ontology.OntologyClass;

import java.io.IOException;
import java.util.*;

public class OntologySerializer extends JsonSerializer<Ontology> {

    public OntologySerializer() {

    }

    @Override
    public void serialize(Ontology ontology, JsonGenerator jsonGenerator, SerializerProvider serializerProvider)
        throws IOException {

        Optional.ofNullable(ontology.getId()).orElseThrow(() -> new IllegalStateException("The id of this Ontology is null"));

        jsonGenerator.writeStartObject();
        jsonGenerator.writeObjectField("_id", ontology.getId().getId());
        jsonGenerator.writeStringField("ontologyName", ontology.getOntologyName());

        // Fetch all ontology classes.
        final SortedSet<OntologyClass> classes = collectOntologyClasses(ontology);
        jsonGenerator.writeObjectField("ontologyClasses", classes);

        // Write root class ids.
        final List<OntologyClass> rootClassIds = new ArrayList<>(ontology.getOntologyChildren());

        jsonGenerator.writeObjectField("rootClasses", rootClassIds);

        jsonGenerator.writeEndObject();
    }

    private SortedSet<OntologyClass> collectOntologyClasses(Ontology o) {
        final SortedSet<OntologyClass> classes = new TreeSet<>();
        o.getOntologyChildren().forEach(child -> traversePath(child, classes));

        return classes;
    }

    private SortedSet<OntologyClass> traversePath(OntologyClass clazz, SortedSet<OntologyClass> res) {
        // Add clazz self
        res.add(clazz);

        // Ensure that all classes have an id.
        Optional.ofNullable(clazz.getId()).orElseThrow(() -> new IllegalStateException("The id of this OntologyClass is null"));

        // Add all subclasses
        if (clazz.getSubClasses() != null) {
            clazz.getSubClasses().forEach(subclass -> traversePath(subclass, res));
        }

        return res;
    }
}
