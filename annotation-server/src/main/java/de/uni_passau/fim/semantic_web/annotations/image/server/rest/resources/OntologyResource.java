/*
 * ==========================License-Start=============================
 * annotation-server : OntologyResource
 *
 * Copyright © 2018 Universität Passau, Germany
 *
 * MIT License
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * ==========================License-End==============================
 */

package de.uni_passau.fim.semantic_web.annotations.image.server.rest.resources;

import de.uni_passau.fim.semantic_web.annotations.image.server.core.AnnotationCore;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.bgtask.BackgroundTask;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.Id;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.ontology.Ontology;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.exceptions.OntologyLoadingException;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.persistence.ontology.OntologyStorage;
import de.uni_passau.fim.semantic_web.annotations.image.server.rest.beans.ErrorMessage;
import de.uni_passau.fim.semantic_web.annotations.image.server.rest.beans.SuccessMessage;
import de.uni_passau.fim.semantic_web.annotations.image.server.rest.beans.ontology.OntologyIdBean;
import de.uni_passau.fim.semantic_web.annotations.image.server.rest.beans.ontology.OntologyUploadResponseBean;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.*;
import java.util.List;
import java.util.stream.Collectors;

/**
 * This resource allows accessing, uploading and deleting ontologies.
 */
@Path("ontology")
public class OntologyResource extends AbstractResource {

    private final Logger log = LoggerFactory.getLogger(this.getClass().getName());
    private final OntologyStorage<Id> ontologyStorage;

    public OntologyResource(AnnotationCore core) {
        super(core);
        this.ontologyStorage = this.core.getOntologyStorage();
    }

    /**
     * Returns a {@link List} of all ontologies stored on the server. Only the IDs of the
     * ontologies are included in this {@link Response}.
     *
     * @return {@link Response} containing the IDs of all ontologies stored on the server.
     */
    @GET
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllOntologies() {
        log.debug(MARKER, "Client requested the ontology list");

        final List<OntologyIdBean> retrievedIds = ontologyStorage.getOntologyList()
            .parallelStream()
            .map(ontology -> {
                log.debug(MARKER, "Retrieved ontology: {}", ontology.getId());
                return new OntologyIdBean(ontology.getId(), ontology.getOntologyName());
            })
            .collect(Collectors.toList());


        return Response
            .ok()
            .entity(retrievedIds)
            .build();
    }

    /**
     * Gets an {@link Ontology} from the backend and returns it as a JSON {@link Response}.
     *
     * @param ontologyId ID of the {@link Ontology} that should get retrieved.
     * @return {@link Response} containing the complete ontology with the requested ID.
     */
    @GET
    @Path("/{id}")
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getOntology(
        @Valid @NotNull @PathParam("id") String ontologyId) {

        log.debug(MARKER, "Client requested the ontology: {}", ontologyId);

        final Id id = this.core.getIdFactory().createId(ontologyId);
        final Ontology retrieved = ontologyStorage.getOntology(id);

        if (retrieved == null) {
            return Response
                .status(Response.Status.NOT_FOUND)
                .entity(new ErrorMessage("This ontology does not exist."))
                .build();
        }

        return Response
            .status(Response.Status.OK)
            .entity(retrieved)
            .build();
    }

    /**
     * Uploads an ontology to the backend.
     *
     * @param ontologyName Name of the ontology to be uploaded. Does not have to be unique.
     * @param ontology     File containing the ontology data.
     * @param fcd          Additional information about the ontology file.
     * @return Returns the backend ID of the uploaded ontology, if the upload has been successful.
     */
    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    public Response uploadOntology(
        @Valid @NotNull @FormDataParam("ontologyName") String ontologyName,
        @Valid @NotNull @FormDataParam("ontology") FormDataBodyPart ontology,
        @Valid @NotNull @FormDataParam("ontology") FormDataContentDisposition fcd) {

        Response.Status status;
        Object entity;

        log.debug(MARKER, "Client requested uploading ontology");

        File ontologyFile = ontology.getValueAs(File.class);

        try (InputStream fis = new FileInputStream(ontologyFile)) {
            final Ontology reqResult = ontologyStorage.uploadOntology(ontologyName, fis);
            status = Response.Status.CREATED;
            entity = new OntologyUploadResponseBean(reqResult.getId());
        } catch (FileNotFoundException e) {
            log.error(MARKER, "The ontology could not be uploaded because the file was not stored correctly.");
            status = Response.Status.INTERNAL_SERVER_ERROR;
            entity = new ErrorMessage("Could not store uploaded ontology file.");
        } catch (OntologyLoadingException e) {
            log.error(MARKER, "The uploaded ontology file could not be loaded: {}", e.getMessage());
            status = Response.Status.BAD_REQUEST;
            entity = new ErrorMessage("The ontology file could not be parsed");
        } catch (IOException e) {
            log.error(MARKER, "Could not create InputStream of uploaded ontology file.");
            status = Response.Status.INTERNAL_SERVER_ERROR;
            entity = new ErrorMessage("The ontology file could not be loaded inside server.");
        }

        return Response
            .status(status)
            .entity(entity)
            .build();
    }

    /**
     * Deletes an ontology from the server.
     *
     * @param ontologyID ID of the {@link Ontology} that should get deleted.
     * @return If the ontology has been deleted successfully, a {@link String} containing the
     * characters "OK" is sent as a {@link Response} to the client. If not, "FAILURE" is
     * sent to the client.
     */
    @DELETE
    @Path("/{id}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response deleteOntology(
        @Valid @NotNull @PathParam("id") String ontologyID) {
        log.debug(MARKER, "Client requests to delete ontology {}", ontologyID);

        final Id id = this.core.getIdFactory().createId(ontologyID);

        final Ontology ontology = ontologyStorage.getOntology(id);

        if (ontology == null) {
            return Response
                .status(Response.Status.NOT_FOUND)
                .entity(new ErrorMessage("This ontology does not exist."))
                .build();
        }

        if (ontologyStorage.deleteOntology(ontology.getId())) {
            log.debug("Ontology with ID {} deleted", ontologyID);
            return Response
                .status(Response.Status.OK)
                .entity(new SuccessMessage(String.format("Successfully deleted ontology %s", ontologyID)))
                .build();
        } else {
            return Response
                .status(Response.Status.INTERNAL_SERVER_ERROR)
                .entity(new ErrorMessage(String.format("Could not delete ontology %s", ontologyID)))
                .build();
        }
    }

    @POST
    @Path("/update/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateOntology(
        @Valid @NotNull @FormDataParam("ontologyId") String ontologyId,
        @Valid @NotNull @FormDataParam("ontology") FormDataBodyPart ontology,
        @Valid @NotNull @FormDataParam("ontology") FormDataContentDisposition fcd) {
        Ontology target = core.getOntologyStorage().getOntology(core.getIdFactory().createId(ontologyId));

        if (target != null) {
            BackgroundTask task = core.getBackgroundTaskManager().enqueue(() -> {
                // CALL UPDATE FUNCTION HERE
            });

            return Response.ok().entity(task).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

}
