/*
 * ==========================License-Start=============================
 * annotation-server : AnnotationRectangleDeserializer
 *
 * Copyright © 2018 Universität Passau, Germany
 *
 * MIT License
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * ==========================License-End==============================
 */

package de.uni_passau.fim.semantic_web.annotations.image.server.core.persistence.image.annotations.mongodb.mapper.deserialize;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.AnnotationCore;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.annotation.shapes.AnnotationRectangle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class AnnotationRectangleDeserializer extends AnnotationDeserializer<AnnotationRectangle> {

    private final Logger log = LoggerFactory.getLogger(AnnotationRectangleDeserializer.class);

    public AnnotationRectangleDeserializer(AnnotationCore core) {
        super(core);
    }

    @Override
    public AnnotationRectangle deserialize(JsonParser jsonParser, DeserializationContext deserializationContext)
        throws IOException {
        // Initial JSON processing.
        final ObjectCodec codec = jsonParser.getCodec();
        final JsonNode root = codec.readTree(jsonParser);

        final AnnotationRectangle.Builder builder = new AnnotationRectangle.Builder();
        super.deserializeBaseAnnotation(builder, root);

        // Add rectangle specific data
        builder.top(root.get("top").asInt());
        builder.left(root.get("left").asInt());
        builder.width(root.get("width").asInt());
        builder.height(root.get("height").asInt());

        builder.scaleX(root.get("scaleX").asDouble());
        builder.scaleY(root.get("scaleY").asDouble());

        return builder.build();
    }
}
