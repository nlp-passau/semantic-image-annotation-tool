/*
 * ==========================License-Start=============================
 * annotation-server : AutomaticAnnotationResource
 *
 * Copyright © 2018 Universität Passau, Germany
 *
 * MIT License
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * ==========================License-End==============================
 */

package de.uni_passau.fim.semantic_web.annotations.image.server.rest.resources;

import de.uni_passau.fim.semantic_web.annotations.image.server.core.AnnotationCore;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.automatic_annotation.model.AutomaticAnnotationAvailableModelTypes;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.automatic_annotation.model.AutomaticAnnotationAvailableModelsForType;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.automatic_annotation.model.AutomaticAnnotationVersion;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.exceptions.UnsuccessfulExecutionException;
import de.uni_passau.fim.semantic_web.annotations.image.server.rest.beans.ErrorMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * This resource is used to communicate with the annotation server. It supports:
 * <ul>
 * <li>Querying the current API version </li>
 * <li>Querying available ClassificationModel types </li>
 * <li>Querying information about the existing image classification model</li>
 * <li>Querying information about the existing text classification model</li>
 * </ul>
 */

@Path("automaticAnnotation")
public class AutomaticAnnotationResource extends AbstractResource {

    private final Logger log = LoggerFactory.getLogger(getClass());

    public AutomaticAnnotationResource(AnnotationCore core) {
        super(core);
    }

    /**
     * Returns current annotation API version.
     *
     * @return {@link String} containing API version.
     */
    @GET
    @Path("/version")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAPIVersion() {

        log.debug("Received version request");

        Response.Status status;
        Object entity;

        try {
            AutomaticAnnotationVersion versionInfo = this.core.getAutomaticAnnotationService()
                .getVersionInfo();

            status = Response.Status.OK;
            entity = versionInfo;

        } catch (UnsuccessfulExecutionException e) {
            log.error("Could not get version info", e);

            status = Response.Status.INTERNAL_SERVER_ERROR;
            entity = new ErrorMessage("Could not get version info.");

        }

        return Response
            .status(status)
            .entity(entity)
            .build();
    }

    /**
     * Returns a list of currently supported model types.
     *
     * @return List of strings of available model types
     */
    @GET
    @Path("/availableModelTypes")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAvailableModelTypes() {

        log.debug("Received available model types request");

        Response.Status status;
        Object entity;

        try {
            AutomaticAnnotationAvailableModelTypes availableModelTypes = this.core.getAutomaticAnnotationService()
                .getAvailableModelTypes();

            status = Response.Status.OK;
            entity = availableModelTypes;

        } catch (UnsuccessfulExecutionException e) {
            log.error("Could not get available model types", e);

            status = Response.Status.INTERNAL_SERVER_ERROR;
            entity = new ErrorMessage("Could not get available model types.");

        }

        return Response
            .status(status)
            .entity(entity)
            .build();
    }

    /**
     * Returns a list of currently supported models for a specific type.
     *
     * @return List of strings of available models for a specific type
     */
    @GET
    @Path("/availableModelsForType/{type}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAvailableModelsForType(@Valid @NotNull @PathParam("type") String type) {

        log.debug("Received available models for a type request");

        Response.Status status;
        Object entity;

        try {
            AutomaticAnnotationAvailableModelsForType availableModelsForType = this.core.getAutomaticAnnotationService()
                .getAvailableModelsForType(type);

            status = Response.Status.OK;
            entity = availableModelsForType;

        } catch (UnsuccessfulExecutionException e) {
            log.error("Could not get available model types", e);

            status = Response.Status.INTERNAL_SERVER_ERROR;
            entity = new ErrorMessage("Could not get available model types.");

        }

        return Response
            .status(status)
            .entity(entity)
            .build();
    }
}
