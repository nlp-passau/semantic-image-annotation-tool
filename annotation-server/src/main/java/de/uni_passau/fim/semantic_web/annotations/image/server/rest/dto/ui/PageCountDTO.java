package de.uni_passau.fim.semantic_web.annotations.image.server.rest.dto.ui;

public class PageCountDTO {
    private long pageCount;

    public PageCountDTO() {
        this.pageCount = 0;
    }

    public PageCountDTO(long pageCount) {
       this.pageCount = pageCount;
    }

    public long getPageCount() {
        return pageCount;
    }

    public void setPageCount(long pageCount) {
        this.pageCount = pageCount;
    }
}
