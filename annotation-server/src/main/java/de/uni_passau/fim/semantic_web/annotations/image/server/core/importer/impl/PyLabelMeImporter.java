/*
 * ==========================License-Start=============================
 * annotation-server : PyLabelMeImporter
 *
 * Copyright © 2018 Universität Passau, Germany
 *
 * MIT License
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * ==========================License-End==============================
 */

package de.uni_passau.fim.semantic_web.annotations.image.server.core.importer.impl;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.AnnotationCore;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.annotation.Annotation;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.annotation.shapes.AnnotationPolygon;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.image.AnnotatedImage;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.ontology.Ontology;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.ontology.OntologyClass;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.importer.Importer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("unused")
public class PyLabelMeImporter extends Importer {

    private static final Logger log = LoggerFactory.getLogger(PyLabelMeImporter.class);

    public static final String DEFAULT_COLOR = "rgba(255,0,0,0.5)";
    public static final String FALLBACK_COLOR = "rgba(255, 0, 255, 0.5)";

    public PyLabelMeImporter(AnnotationCore core) {
        super(core);
    }

    @Override
    public AnnotatedImage importAnnotation(byte[] imageData, ObjectNode annotationData, Ontology targetOntology) {
        return importAnnotation(imageData, annotationData, targetOntology, "IMPORT");
    }

    @Override
    public AnnotatedImage importAnnotation(byte[] imageData, ObjectNode annotationData, Ontology targetOntology,
                                 String imgName) {

        // Convert json data to Annotation objects.
        ArrayNode shapes = (ArrayNode) annotationData.get("shapes");
        List<Annotation> annotations = new ArrayList<>();

        log.debug(MARKER, "Will import {} shapes for one image", shapes.size());

        shapes.forEach(shape -> {
            final AnnotationPolygon.Builder builder = new AnnotationPolygon.Builder();

            final List<AnnotationPolygon.PolygonPoint> points = new ArrayList<>();
            ArrayNode jsonPoints = (ArrayNode) shape.get("points");

            jsonPoints.forEach(pt -> {
                AnnotationPolygon.PolygonPoint polygonPoint = new AnnotationPolygon.PolygonPoint();

                polygonPoint.setX(pt.get(0).asInt());
                polygonPoint.setY(pt.get(1).asInt());

                points.add(polygonPoint);
            });

            builder.points(points);

            // Calculate additional properties required for fabric js.
            @SuppressWarnings("ConstantConditions") AnnotationPolygon.PolygonPoint upperPoint = points
                    .stream()
                    .min(AnnotationPolygon.PolygonPoint::compareTo)
                    .get();

            @SuppressWarnings("ConstantConditions") AnnotationPolygon.PolygonPoint lowerPoint = points
                    .stream()
                    .max(AnnotationPolygon.PolygonPoint::compareTo)
                    .get();

            // Find the leftmost and rightmost point of the polygon.
            AnnotationPolygon.PolygonPoint left = points.get(0);
            AnnotationPolygon.PolygonPoint right = points.get(0);

            for (final AnnotationPolygon.PolygonPoint p : points) {
                if (p.getX() < left.getX()) {
                    left = p;
                }

                if (p.getX() > right.getX()) {
                    right = p;
                }
            }

            OntologyClass targetClass = null;
            // Fetch ontology data.
            if (!(shape.get("label") == null)) {
                targetClass = this.core.getOntologyStorage()
                    .getOntologyClassByName(targetOntology.getId(), shape.get("label").asText());
            }

            if (targetClass == null) {
                log.warn(MARKER, "The given label '{}' doesn't match the ontology: {}", shape.get("label").asText(),
                    targetOntology.getId());
                builder.fill(FALLBACK_COLOR);
            } else {
                builder.fill(DEFAULT_COLOR);
            }

            builder.ontology(targetOntology);
            builder.ontologyClass(targetClass);
            builder.id(this.core.getIdFactory().createId());

            annotations.add(builder.build());
        });

        return core.getImageStorage().uploadImage(imageData, annotations, imgName);
    }

    @Override
    public String getName() {
        return "PyLabelMe";
    }
}
