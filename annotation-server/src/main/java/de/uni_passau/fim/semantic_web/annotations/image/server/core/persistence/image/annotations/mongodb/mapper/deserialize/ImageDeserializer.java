/*
 * ==========================License-Start=============================
 * annotation-server : ImageDeserializer
 *
 * Copyright © 2018 Universität Passau, Germany
 *
 * MIT License
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * ==========================License-End==============================
 */

package de.uni_passau.fim.semantic_web.annotations.image.server.core.persistence.image.annotations.mongodb.mapper.deserialize;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.AnnotationCore;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.image.Image;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.ontology.FlatOntology;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.persistence.image.annotations.mongodb.MongoId;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.persistence.image.annotations.mongodb.mapper.AnnotationMongoModule;
import org.bson.types.ObjectId;

import java.io.IOException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Optional;

public class ImageDeserializer<T extends Image> extends JsonDeserializer<T> {

    protected final AnnotationCore core;
    protected ObjectMapper mapper;

    public ImageDeserializer(final AnnotationCore core) {
        this.core = core;
    }

    protected ObjectMapper getMapper() {
        if (this.mapper == null) {
            this.mapper = new ObjectMapper();
            mapper.registerModule(new AnnotationMongoModule(core));
        }
        return this.mapper;
    }

    @Override
    public T deserialize(final JsonParser jsonParser,
                         final DeserializationContext deserializationContext)
        throws IOException {
        final ObjectCodec codec = jsonParser.getCodec();
        final JsonNode root = codec.readTree(jsonParser);

        return deserializeSuper(root);
    }

    public T deserializeSuper(JsonNode root) throws IOException {
        // Search for the correct Builder.
        Image.Builder builder = null;

        Type target = ((ParameterizedType) getClass()
            .getGenericSuperclass()).getActualTypeArguments()[0];

        Class<T> builderParent = target.getTypeName().equals("T") ?
            (Class<T>) Image.class : (Class<T>) target;

        Optional<Class<?>> builderClass = Arrays.stream(builderParent.getDeclaredClasses()).filter(clazz -> {
            if (clazz.getSimpleName().equals("Builder")) {
                return true;
            } else {
                return false;
            }
        }).findFirst();

        if (builderClass.isPresent()) {
            try {
                builder = (Image.Builder) builderClass.get().newInstance();
            } catch (IllegalAccessException|InstantiationException e) {
                e.printStackTrace();
            }
        }

        builder.id(new MongoId(this.getMapper().treeToValue(root.get("_id"), ObjectId.class)));
        builder.size(new Image.Size(root.get("size").get("width").asInt(),
            root.get("size").get("height").asInt()));
        builder.filename(root.get("filename").asText());

        return (T) builder.build();
    }
}
