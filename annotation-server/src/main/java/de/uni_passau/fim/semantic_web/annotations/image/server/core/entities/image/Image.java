/*
 * ==========================License-Start=============================
 * annotation-server : Image
 *
 * Copyright © 2018 Universität Passau, Germany
 *
 * MIT License
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * ==========================License-End==============================
 */

package de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.image;

import com.fasterxml.jackson.annotation.JsonProperty;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.Entity;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.EntityBuilder;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.Id;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.annotation.Annotation;

import javax.validation.constraints.NotNull;

/**
 * This class defines the meta information of an image stored on the server. Besides the raw image
 * data, images have the following attributes:
 *
 * <ul>
 * <li>Image ID (id obtained through inheritance from {@link Entity} class</li>
 * <li>{@link Annotation}s</li>
 * </ul>
 */
public class Image extends Entity<Id> {
    @NotNull
    @JsonProperty("size")
    protected Image.Size size;

    @NotNull
    @JsonProperty("filename")
    protected String filename;

    protected Image(Image.Builder builder) {
        super(builder);
        this.size = builder.size;
        this.filename = builder.filename;
    }

    public Size getSize() {
        return size;
    }

    public String getFilename() {
        return filename;
    }

    public static class Builder<B extends Image.Builder<B, T>, T extends Image>
        extends EntityBuilder<Id, T> {
        protected Image.Size size;
        protected String filename;

        public B size(Image.Size size) {
            this.size = size;
            return (B) this;
        }

        public B filename(String filename) {
            this.filename = filename;
            return (B) this;
        }

        @Override
        public T build() {
            return (T) new Image(this);
        }
    }

    public static class Size {
        private final int width;
        private final int height;

        public Size(int width, int height) {
            this.width = width;
            this.height = height;
        }

        public int getWidth() {
            return width;
        }

        public int getHeight() {
            return height;
        }
    }
}
