/*
 * ==========================License-Start=============================
 * annotation-server : AnnotationServer
 *
 * Copyright © 2018 Universität Passau, Germany
 *
 * MIT License
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * ==========================License-End==============================
 */

package de.uni_passau.fim.semantic_web.annotations.image.server;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.AnnotationCore;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.AnnotationCoreService;
import de.uni_passau.fim.semantic_web.annotations.image.server.rest.AnnotationRESTServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

/**
 * The main class of this application.
 * <p>
 * Initializes the backend as well as the REST server.
 */
public class AnnotationServer {

    /**
     * Logger of this class.
     */
    private static final Logger LOG = LoggerFactory.getLogger(AnnotationServer.class);
    private static final Marker MARKER = MarkerFactory.getMarker("Server");

    /**
     * Runs the application.
     *
     * @param args Ignored.
     */
    public static void main(String[] args) {
        final Config config = ConfigFactory.load().getConfig("annotation");
        final AnnotationCore core = new AnnotationCoreService(config);
        run(core);
    }

    /**
     * Starts the REST server.
     */
    private static void run(AnnotationCore core) {
        final AnnotationRESTServer restServer = new AnnotationRESTServer(core);
        try {
            restServer.start();
            LOG.info(MARKER, "Server started");
        } catch (Exception e) {
            LOG.error("The server could not be started: '{}' -> '{}'",
                e.getClass().getName(), e.getMessage());
            restServer.stop();
            return;
        }

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            LOG.debug(MARKER, "Shutting down...");
            restServer.stop();
        }));
    }
}
