/*
 * ==========================License-Start=============================
 * annotation-server : OntologyMongoModule
 *
 * Copyright © 2018 Universität Passau, Germany
 *
 * MIT License
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * ==========================License-End==============================
 */

package de.uni_passau.fim.semantic_web.annotations.image.server.core.persistence.ontology.mongodb.mapper;

import com.fasterxml.jackson.core.json.PackageVersion;
import com.fasterxml.jackson.databind.module.SimpleModule;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.ontology.FlatOntology;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.ontology.Ontology;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.ontology.OntologyClass;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.persistence.ontology.mongodb.mapper.deserialize.FlatOntologyDeserializer;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.persistence.ontology.mongodb.mapper.deserialize.OntologyClassDeserializer;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.persistence.ontology.mongodb.mapper.deserialize.OntologyDeserializer;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.persistence.ontology.mongodb.mapper.serialize.OntologyClassSerializer;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.persistence.ontology.mongodb.mapper.serialize.OntologySerializer;

public class OntologyMongoModule extends SimpleModule {

    public OntologyMongoModule() {
        super(PackageVersion.VERSION);

        // Register (de)serializer for Ontology class.
        addDeserializer(FlatOntology.class, new FlatOntologyDeserializer<>());
        addSerializer(Ontology.class, new OntologySerializer());
        addDeserializer(Ontology.class, new OntologyDeserializer());

        // Register (de)serializer for OntologyClass class.
        addSerializer(OntologyClass.class, new OntologyClassSerializer());
        addDeserializer(OntologyClass.class, new OntologyClassDeserializer());
    }

    @Override
    public String getModuleName() {
        return getClass().getSimpleName();
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }
}
