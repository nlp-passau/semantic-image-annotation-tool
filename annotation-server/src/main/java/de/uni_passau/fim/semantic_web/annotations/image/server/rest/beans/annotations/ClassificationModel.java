/*
 * ==========================License-Start=============================
 * annotation-server : ClassificationModel
 *
 * Copyright © 2018 Universität Passau, Germany
 *
 * MIT License
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * ==========================License-End==============================
 */

package de.uni_passau.fim.semantic_web.annotations.image.server.rest.beans.annotations;

public class ClassificationModel {

    private String description;
    private String longName;
    private String modelId;
    private String name;

    private ClassificationModel(ClassificationModel.Builder builder) {
        this.modelId = builder.modelId;
        this.name = builder.name;
        this.longName = builder.longName;
        this.description = builder.description;
    }

    public String getModelId() {
        return modelId;
    }

    public String getName() {
        return name;
    }

    public String getLongName() {
        return longName;
    }

    public String getDescription() {
        return description;
    }

    public static class Builder {

        String modelId;
        String name;
        String longName;
        String description;

        public ClassificationModel.Builder modelId(String modelId) {
            this.modelId = modelId;
            return this;
        }

        public ClassificationModel.Builder name(String name) {
            this.name = name;
            return this;
        }

        public ClassificationModel.Builder longName(String longName) {
            this.longName = longName;
            return this;
        }

        public ClassificationModel.Builder description(String description) {
            this.description = description;
            return this;
        }

        public ClassificationModel build() {
            if (this.modelId == null) {
                throw new IllegalStateException("modelId == null");
            }

            if (this.name == null) {
                throw new IllegalStateException("name == null");
            }

            if (this.longName == null) {
                throw new IllegalStateException("longName == null");
            }

            if (this.description == null) {
                throw new IllegalStateException("description == null");
            }

            return new ClassificationModel(this);
        }
    }
}
