/*
 * ==========================License-Start=============================
 * annotation-server : ImportManager
 *
 * Copyright © 2018 Universität Passau, Germany
 *
 * MIT License
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * ==========================License-End==============================
 */

package de.uni_passau.fim.semantic_web.annotations.image.server.core.importer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.AnnotationCore;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.AnnotationCoreServiceFeature;
import de.uni_passau.fim.semantic_web.annotations.image.server.core.entities.ontology.Ontology;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.util.*;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * The {@link ImportManager} class contains all logic regarding importing existing annotations
 * made in other annotation software into annotation-core.
 */
public class ImportManager extends AnnotationCoreServiceFeature {
    private static final String[] SUPPORTED_IMAGE_FILES = {".jpg", ".jpeg", ".png"};

    private final static ObjectMapper MAPPER = new ObjectMapper();
    private final Logger log = LoggerFactory.getLogger(ImportManager.class);

    private Map<String, Importer> availableImporters;

    public ImportManager(AnnotationCore core) {
        super(core);

        this.availableImporters = new HashMap<>();

        // Register available importers.
        for (String importerClassName : core.getConfig().getStringList("importer")) {
            log.debug("Will try to register import: {}", importerClassName);
            try {
                Class<?> importerClazz = Class.forName(importerClassName);
                Constructor<?> constructor = importerClazz.getConstructors()[0];
                registerImporter((Importer) constructor.newInstance(core));
                log.info("Importer {} successfully imported.", importerClassName);
            } catch (ReflectiveOperationException e) {
                log.error("Failed to initialize Importer: {}", importerClassName);
                log.info("Exception:", e);
            }
        }
    }

    private static boolean endsWithAny(String s, String[] suffixes) {
        for (String suffix : suffixes) {
            if (s.endsWith(suffix)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Imports existing annotations from a zip file to annotation-core.
     */
    public void importAnnotations(Importer i, ZipFile data, Ontology targetOntology) {
        Map<ZipEntry, ZipEntry> imgJsonPairs = prepare(data);

        imgJsonPairs.forEach((key, value) -> {
            try (InputStream jsonStream = data.getInputStream(key);
                 InputStream imgStream = data.getInputStream(value)) {

                byte[] imgData = IOUtils.toByteArray(imgStream);

                i.importAnnotation(imgData, (ObjectNode) MAPPER.readTree(jsonStream), targetOntology, value.getName());
            } catch (IOException e) {
                log.error("Failed to import annotations.");
                log.info("Exception:", e);
            }
        });
    }

    private Map<ZipEntry, ZipEntry> prepare(ZipFile data) {
        // Look at all files stored inside the zip file and note the zip entries down into a list.
        List<ZipEntry> zipEntries = data.stream().collect(Collectors.toList());

        // Now look for all json files.
        List<ZipEntry> jsonFiles = zipEntries
            .stream()
            .filter(entry -> !entry.isDirectory() && entry.getName().endsWith(".json"))
            .collect(Collectors.toList());

        // Look for all image files.
        List<ZipEntry> imageFiles = zipEntries
            .stream()
            .filter(entry -> !entry.isDirectory() && endsWithAny(entry.getName(), SUPPORTED_IMAGE_FILES))
            .collect(Collectors.toList());

        // Now map all json entries to the image data entries.
        final Map<ZipEntry, ZipEntry> mapped = new HashMap<>();
        jsonFiles.forEach(json -> {
            // Find corresponding image zip entry.
            String targetName = json.getName().substring(0, json.getName().length() - "json".length());

            Optional<ZipEntry> imgTarget = imageFiles
                .stream()
                .filter(imgFile -> imgFile.getName().startsWith(targetName))
                .findFirst();

            imgTarget.ifPresent(zipEntry -> mapped.put(json, zipEntry));
        });

        return mapped;
    }

    /**
     * Registers a new {@link Importer} inside this {@link ImportManager}.
     *
     * @param i Instance of {@link Importer} that should get registered.
     */
    private void registerImporter(Importer i) {
        availableImporters.put(i.getName(), i);
    }

    public Importer getImporterByName(String importerName) {
        return availableImporters
            .entrySet()
            .stream()
            .filter(e -> e.getKey().equals(importerName))
            .map(Map.Entry::getValue)
            .findFirst()
            .orElse(null);
    }

    public Collection<Importer> getAvailableImporters() {
        return Collections.unmodifiableCollection(availableImporters.values());
    }
}
