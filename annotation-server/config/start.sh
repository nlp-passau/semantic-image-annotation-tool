#!/bin/sh

ROOTDIR=`pwd`
CONFDIR="${ROOTDIR}/config"
LIBDIR="${ROOTDIR}/lib"

JAVA_OPTS=""

cmd="java -jar -server ${JAVA_OPTS} \
	-Djava.net.preferIPv4Stack=true \
	-Dfile.encoding=UTF-8 \
	-Dlogback.configurationFile=${CONFDIR}/logback.xml \
	-Dconfig.file=${CONFDIR}/application.conf \
	${LIBDIR}/${project.artifactId}-${project.version}.jar"

exec ${cmd}
