annotation-server REST Routes draft:

Regarding the Ontology:
/ontology/$ontologyID - GET
/ontology/ - POST (Body: Link to an ontology in RDF format)
/ontology/ - PUT (Uploading a new ontology to the server)
/ontology/$ontologyID - UPDATE (Updating an existing ontology on the server)

Regarding the annotations/images:
/image/$id - GET (Returns an image)
/image/  -  PUT (Uploads an image to the server)
/image/$id - DELETE (Deletes an image from the server)
/image/$id/annotations - GET (Returns the annotations related to an image with id $id)
/image/$id/annotations - POST (POSTs annotations to the server, json ontologyClass)
/images/$limit$page$searchParams - GET (Returns a list of images on the server, with thumbnails)

annotation-server data storage draft:
We'll use a mongodb database.
