'use strict';

/**
 * @ngdoc directive
 * @name AnnotationToolApp.directive:fileLoad
 * @description
 * # fileLoad
 */
angular.module('AnnotationToolApp')
	.directive('fileLoad', function () {
		return {
			restrict: 'A',
			scope: {
				'fileLoad': '='
			},
			link: function (scope, element) {
				element.bind('change', function (changeEvent) {
					var reader = new FileReader();
					reader.onload = function (loadEvent) {
						scope.$apply(function () {
							scope.fileLoad = loadEvent.target.result;
						});
					};
					reader.readAsDataURL(changeEvent.target.files[0]);
				});
			}
		};
	});
