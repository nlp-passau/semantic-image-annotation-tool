'use strict';

/**
 * @ngdoc directive
 * @name AnnotationToolApp.directive:ontologyManager
 * @description
 * # ontologyManager
 */
angular.module('AnnotationToolApp')
	.directive('ontologyManager', ['Configuration', 'FileUploader', 'OntologyFactory', function (Configuration, FileUploader, OntologyFactory) {
		return {
			templateUrl: 'views/templates/ontology_manager.tpl.html',
			restrict: 'E',
			scope: {},
			link: function (scope) {

				scope.uploader = new FileUploader({
					url: Configuration.ontology,
					removeAfterUpload: true,
					alias: 'ontology'
				});

				scope.uploader.onSuccessItem = function () {
					scope.getOntologies();
				};

				scope.uploader.onAfterAddingFile = function (fileItem) {
					if (scope.uploader.getNotUploadedItems().length > 1) {
						scope.uploader.removeFromQueue(0);
					}
					fileItem.formData.push({ontologyName: fileItem.file.name});
				};

				scope.getOntologies = function () {

					scope.ontologyLoading = true;

					OntologyFactory
						.getAllOntologies()
						.$promise
						.then(
							success => {
								scope.ontologies = success;
							},
							error => {
								console.log(error);
							})
						.finally(() => {
							scope.ontologyLoading = false;
						});
				};

				scope.removeOntology = function (ontologyId) {

					OntologyFactory
						.deleteOntology({id: ontologyId})
						.$promise
						.then(
							success => {
								scope.getOntologies();
							},
							error => {
								console.log(error);
							}
						)
						.finally();
				};

				scope.uploadOntology = function () {
					scope.uploader.uploadAll();
				};

				scope.getOntologies();
			}
		};
	}]);
