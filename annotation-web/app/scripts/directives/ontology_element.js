'use strict';

/**
 * @ngdoc directive
 * @name AnnotationToolApp.directive:ontologyElement
 * @description
 * # ontologyElement
 */
angular.module('AnnotationToolApp')
	.directive('ontologyElement', function () {
		return {
			template: '<div class="ontologyElement"><div class="tree-toggle" ng-click="toggle();$event.stopPropagation();">' +
			'<i ng-if="ontologyClass.subClasses.length > 0 && isCollapsed" class="fas fa-plus"></i>' +
			'<i ng-if="ontologyClass.subClasses.length > 0 && !isCollapsed" class="fas fa-minus"></i>' +
			'<i ng-if="ontologyClass.subClasses.length <= 0;" class="fas fa-tag"></i></div>' + // leaf
			'<div class="tree-element" ng-click="clickHandler(ontologyClass)" ng-dblclick="toggleinfo();$stopPropagation();" ' +
			'data-toggle="tooltip" data-placement="bottom" data-html="true" title="{{ontologyClass.literals | ontologyelementinfo}}">' +
			'{{ localizedLiteral }}' +
			'</div>' +
			'<div class="element-info" ng-if="this.showinfo">' +
			'<p><b>Class:</b> {{ontologyClass.className}}</p>' +
			'<div class="info-literal"><b>Literals:</b> {{ontologyClass.literals | ontologyelementinfo}}</div> ' + //todo filter empty lists
			'<div class="info-comment"><b>Comments:</b> {{ontologyClass.comments | ontologyelementinfo}}</div> ' +
			'</div>' +
			'<ul>' +
			'<li ng-if="!isCollapsed" ng-repeat="child in ontologyClass.subClasses | orderBy:getLocalizedLiteral">' +
			'<ontology-element click-handler="clickHandler" ontology-class="child" selected-language="selectedLanguage"></ontology-element>' +
			'</li>' +
			'</ul>' +
			'</div>',
			restrict: 'E',
			scope: {
				ontologyClass: '=',
				selectedLanguage: '=',
				clickHandler: '='
			},
			link: function postLink(scope, element, attrs) {
				let self = this;
				let showinfo = true;

				scope.localizedLiteral = scope.ontologyClass.className;

				// Collapse per default.
				element.toggleClass('collapsed');
				scope.isCollapsed = true;

				scope.$on('ontologyViewer:expand', function(evt, data) {
					scope.toggle();
				})

				scope.toggle = function () {
					element.toggleClass('collapsed');
					scope.isCollapsed = !scope.isCollapsed;
				};

				scope.toggleinfo = function () {
					this.showinfo = !this.showinfo;
				};

				scope.$watch('selectedLanguage', function() {
					scope.localizedLiteral = scope.getLocalizedLiteral(scope.ontologyClass);
				}, true);

				/**
				 * Sets the object name in the given language. Takes the english or class name as fallback.
				 * @returns {*}
				 */
				scope.getLocalizedLiteral = function(obj) {
					if (obj.literals[scope.selectedLanguage.language]) {
						return obj.literals[scope.selectedLanguage.language];
					} else if (obj.literals.en) {
						return obj.literals.en;
					} else {
						return obj.className;
					}
				};
			}
		};
	});
