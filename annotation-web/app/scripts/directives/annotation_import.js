'use strict';

/**
 * @ngdoc directive
 * @name AnnotationToolApp.directive:annotationImport
 * @description
 * # annotationImport
 */
angular.module('AnnotationToolApp')
	.directive('annotationImport', ['Configuration', 'FileUploader', 'Notification', 'OntologyFactory', 'ImportFactory',
		function (Configuration, FileUploader, Notification, OntologyFactory, ImportFactory) {
			return {
				templateUrl: 'views/templates/annotation_import.tpl.html',
				restrict: 'E',
				link: function postLink(scope, element, attrs) {
					ImportFactory
						.getAvailableImporters()
						.$promise
						.then(
							success => {
								scope.availableImporters = success;
							},
							error => {
								console.log('Could not fetch available importers: ', error);
							}
						);

					OntologyFactory
						.getAllOntologies()
						.$promise
						.then(
							success => {
								scope.availableOntologies = success;
							},
							error => {
								console.log('Could not fetch available ontologies.')
							}
						);

					scope.uploader = new FileUploader({
						url: Configuration.import,
						removeAfterUpload: true
					});

					scope.uploadAll = function () {
						let err = false;

						scope.uploader.getNotUploadedItems().forEach(function (item) {
							if (!item.selectedImporter) {
								err = true;
								item.importerMissing = true;
							}
						});

						if (err) {
							Notification.error('Oops, looks like you haven\'t selected an importer for a archive that should be imported!');
						} else {
							scope.uploader.uploadAll();
						}
					};

					scope.uploader.onBeforeUploadItem = function (item) {
						if (!item.selectedImporter) {
							Notification.error('Oops, looks like you haven\'t selected an importer for a archive that should be imported!');
							uploader.cancelItem(item);
						}

						item.formData.push({
							'selectedImporter': item.selectedImporter.name,
							'ontologyId': item.selectedOntology.id
						});
					}
				}
			};
		}]);
