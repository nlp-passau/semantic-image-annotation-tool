'use strict';

/**
 * @ngdoc service
 * @name AnnotationToolApp.ServerInfoFactory
 * @description
 * # ServerInfoFactory
 * Factory in the AnnotationToolApp.
 */
angular.module('AnnotationToolApp')
	.factory('ServerInfoFactory', ($resource, Configuration) => {

		return $resource(
			Configuration.admin.serverInfo,
			{}, // no default parameters
			{
				getServerInfo: {
					method: 'GET'
				}
			}
		);


	});
