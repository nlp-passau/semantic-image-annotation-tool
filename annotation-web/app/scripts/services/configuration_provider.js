'use strict';

/**
 * @ngdoc provider
 * @name AnnotationToolApp.ConfigurationProvider
 * @description
 * # ConfigurationProvider
 * Provider in the AnnotationToolApp.
 */
angular.module('AnnotationToolApp')
	.provider('Configuration', [function () {
		let ConfigurationData;
		this.initialize = (data) => {
			ConfigurationData = data;
		};
		this.$get = () => {
			return ConfigurationData;
		};
	}]);