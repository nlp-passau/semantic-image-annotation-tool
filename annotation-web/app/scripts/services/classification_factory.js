'use strict';

/**
 * @ngdoc service
 * @name AnnotationToolApp.ClassificationFactory
 * @description
 * # ClassificationFactory
 * Factory in the AnnotationToolApp.
 */
angular.module('AnnotationToolApp')
	.factory('ClassificationFactory', ($resource, Configuration) => {

		return $resource(
			Configuration.classification,
			{}, // no default parameters
			{
				getAvailableModels: {
					url: Configuration.classification.availableModelsForType,
					method: 'GET',
					isArray: true
				},
			}
		);


	});
