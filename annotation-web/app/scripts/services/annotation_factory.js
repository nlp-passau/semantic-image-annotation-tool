'use strict';

/**
 * @ngdoc service
 * @name AnnotationToolApp.AnnotationFactory
 * @description
 * # AnnotationFactory
 * Factory in the AnnotationToolApp.
 */
angular.module('AnnotationToolApp')
	.factory('AnnotationFactory', ($resource, Configuration) => {

		return $resource(
			Configuration.image,
			{}, // no default parameters
			{
				getAllImages: {
					method: 'GET',
					url: Configuration.image,
					isArray: true
				},

				getAnnotations: {
					method: 'GET',
					url: Configuration.image + '/:id/annotations'
				},

				updateAnnotations: {
					method: 'PUT',
					url: Configuration.image + '/:id/annotations'
				},

				deleteImage: {
					method: 'DELETE',
					url: Configuration.image + '/:id'
				}
			}
		);


	});
