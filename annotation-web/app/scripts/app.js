'use strict';

/**
 * @ngdoc overview
 * @name AnnotationToolApp
 * @description
 * # AnnotationToolApp
 *
 * Main module of the application.
 */
angular
	.module(
		'AnnotationToolApp', 
		[
			'ngAnimate',
			'ngCookies',
			'ngResource',
			'ngRoute',
			'ngSanitize',
			'ui-notification',
			'ngFilesizeFilter',
			'angularFileUpload',
			'angularjs-dropdown-multiselect',
			'ui.bootstrap',
			'angularModalService'
		])
	.config(($routeProvider) => {
		$routeProvider
			.when('/browse/:page?', {
				templateUrl: 'views/browse.html',
				controller: 'BrowseCtrl',
				controllerAs: 'browseCtrl'
			})
			.when('/annotations/:id?', {
				templateUrl: 'views/annotations.html',
				controller: 'AnnotationsCtrl',
				controllerAs: 'annotationsCtrl'
			})
			.when('/admin', {
				templateUrl: 'views/admin.html',
				controller: 'AdminCtrl',
				controllerAs: 'adminCtrl'
			})
			.otherwise({
				redirectTo: '/browse'
			});
	})
	.config(['$httpProvider', ($httpProvider) => $httpProvider.interceptors.push('AuthenticationInterceptor')])
	.config(['$logProvider', 'ConfigurationProvider', ($logProvider, ConfigurationProvider) => {
		$logProvider.debugEnabled(ConfigurationProvider.debugEnabled);
	}])
;