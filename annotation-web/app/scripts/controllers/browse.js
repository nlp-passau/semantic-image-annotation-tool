'use strict';

/**
 * @ngdoc function
 * @name AnnotationToolApp.controller:BrowseCtrl
 * @description
 * # BrowseCtrl
 * Controller of the AnnotationToolApp
 */
angular.module('AnnotationToolApp')
	.controller('BrowseCtrl', ['$scope', '$routeParams' , 'Notification', 'AnnotationFactory', 'Configuration',
	 function ($scope, $routeParams, Notification, AnnotationFactory, Configuration) {
		let self = this;
		self.imageUrl = Configuration.image;
		self.loadingImages = true;

		let page = $routeParams.page ? $routeParams.page : 1;
		let limit = 20; // TODO: Perhaps offer a function for the user to change this.

		AnnotationFactory
			.getAllImages({page: page, limit: limit})
			.$promise
			.then(
				success => {
					self.images = success;
				},
				error => {
					console.log(error);
					Notification.error('Could not get all images: ' + error.code);
				})
			.finally(() => {
				self.loadingImages = false;
			});
	}]);
