'use strict';

/**
 * @ngdoc function
 * @name AnnotationToolApp.controller:YesNoModal
 * @description
 * # AdminCtrl
 * Controller of the AnnotationToolApp
 */
angular.module('AnnotationToolApp').controller('YesNoModal',
	function ($scope, close, title, text, yesText, noText, yesButtonClass, noButtonClass) {
		$scope.title = title;
		$scope.text = text;
		$scope.yesText = (yesText) ? yesText : "Yes";
		$scope.noText = (noText) ? noText : "No";
		$scope.yesButtonClass = (yesButtonClass) ? yesButtonClass : "btn-success";
		$scope.noButtonClass = (noButtonClass) ? noButtonClass : "btn-danger";

		$scope.dismiss = function(res) {
            close(res, 200);
        }
	});
