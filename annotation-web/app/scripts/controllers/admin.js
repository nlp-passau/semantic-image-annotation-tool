'use strict';

/**
 * @ngdoc function
 * @name AnnotationToolApp.controller:AdminCtrl
 * @description
 * # AdminCtrl
 * Controller of the AnnotationToolApp
 */
angular.module('AnnotationToolApp').controller('AdminCtrl', [
	function () {
		let self = this;

		self.serverInfo = {
			availableCPUs: 0,
			freeMemory: 0,
			allocatedMemory: 0,
			maxMemory: 0,
			load: 0,
			osName: '',
			osVersion: '',
			arch: ''
		};

	}]);
