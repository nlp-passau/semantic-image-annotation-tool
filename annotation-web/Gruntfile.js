'use strict';

module.exports = function (grunt) {

	// Time how long tasks take. Can help when optimizing build times
	require('time-grunt')(grunt);

	// Automatically load required Grunt tasks
	require('jit-grunt')(grunt, {
		useminPrepare: 'grunt-usemin',
		ngtemplates: 'grunt-angular-templates'
	});

	grunt.loadNpmTasks('grunt-nginclude');
	grunt.loadNpmTasks('grunt-stylelint');
	grunt.loadNpmTasks('grunt-contrib-compress');

	require('load-grunt-tasks')(grunt);

	// Configurable paths for the application
	const appConfig = {
		app: require('./bower.json').appPath || 'app',
		dist: 'dist',
		name: 'AnnotationToolApp'
	};

	const serveStatic = require('serve-static');
	const sass = require('node-sass');

	// Define the configuration for all the tasks
	grunt.initConfig({

		// Watches files for changes and runs tasks based on the changed files
		watch: {

			bower: {
				files: ['bower.json'],
				tasks: ['wiredep']
			},

			js: {
				files: ['app/scripts/{,*/}*.js'],
				tasks: ['newer:jshint:all', 'newer:jscs:all'],
				options: {
					livereload: '<%= connect.options.livereload %>'
				}
			},

			gruntfile: {
				files: ['Gruntfile.js']
			},

			livereload: {
				options: {
					livereload: '<%= connect.options.livereload %>'
				},
				files: [
					'app/{,*/}*.html',
					'.tmp/styles/{,*/}*.css',
					'app/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}'
				]
			}
		},

		// The actual grunt server settings
		connect: {
			options: {
				port: 9000,
				// Change this to '0.0.0.0' to access the server from outside.
				hostname: 'localhost',
				livereload: 35729
			},
			livereload: {
				options: {
					open: false,
					middleware: function (connect) {
						return [
							serveStatic('.tmp'),
							connect().use(
								'/bower_components',
								serveStatic('./bower_components')
							),
							connect().use(
								'/app/styles',
								serveStatic('./app/styles')
							),
							connect().use(
								'/fonts',
								serveStatic('./bower_components/font-awesome/webfonts')
							),
							serveStatic('app/appConfig.json'),
							serveStatic(appConfig.app)
						];
					}
				}
			},

			dist: {
				options: {
					open: true,
					base: 'dist'
				}
			}
		},

		// Make sure there are no obvious mistakes
		jshint: {
			options: {
				jshintrc: '.jshintrc',
				reporter: require('jshint-stylish')
			},
			all: {
				src: [
					'Gruntfile.js',
					'app/scripts/{,*/}*.js'
				]
			}
		},

		// Make sure code styles are up to par
		jscs: {
			src: 'app/scripts/{,*/}*.js',
			options: {
				config: '.jscsrc',
				verbose: true
			},
			all: {
				src: [
					'Gruntfile.js',
					'app/scripts/{,*/}*.js'
				]
			}
		},

		// Empties folders to start fresh
		clean: {
			dist: {
				files: [{
					dot: true,
					src: [
						'.tmp',
						'dist/{,*/}*',
						'!dist/.git{,*/}*'
					]
				}]
			},
			server: '.tmp'
		},

		// Add vendor prefixed styles
		postcss: {
			options: {
				processors: [
					require('autoprefixer')({browsers: ['last 2 versions']})
				]
			},
			server: {
				options: {
					map: true
				},
				files: [{
					expand: true,
					cwd: '.tmp/styles/',
					src: '{,*/}*.css',
					dest: '.tmp/styles/'
				}]
			},
			dist: {
				files: [{
					expand: true,
					cwd: '.tmp/styles/',
					src: '{,*/}*.css',
					dest: '.tmp/styles/'
				}]
			}
		},

		// Automatically inject Bower components into the app
		wiredep: {
			app: {
				src: ['app/index.html'],
				ignorePath: /\.\.\//
			},
			sass: {
				src: ['app/styles/{,*/}*.{scss,sass}'],
				ignorePath: /(\.\.\/){1,2}bower_components\//
			}
		},

		// Renames files for browser caching purposes
		filerev: {
			dist: {
				src: [
					'dist/scripts/{,*/}*.js',
					'dist/styles/{,*/}*.css',
					'dist/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}',
					'dist/styles/fonts/*'
				]
			}
		},

		// Reads HTML for usemin blocks to enable smart builds that automatically
		// concat, minify and revision files. Creates configurations in memory so
		// additional tasks can operate on them
		useminPrepare: {
			html: 'app/index.html',
			options: {
				dest: 'dist',
				flow: {
					html: {
						steps: {
							js: ['concat', 'uglifyjs'],
							css: ['cssmin']
						},
						post: {}
					}
				}
			}
		},

		// Performs rewrites based on filerev and the useminPrepare configuration
		usemin: {
			html: ['dist/{,*/}*.html'],
			css: ['dist/styles/{,*/}*.css'],
			js: ['dist/scripts/{,*/}*.js'],
			options: {
				assetsDirs: [
					'dist',
					'dist/images',
					'dist/styles'
				],
				patterns: {
					js: [[/(images\/[^''""]*\.(png|jpg|jpeg|gif|webp|svg))/g, 'Replacing references to images']]
				}
			}
		},

		imagemin: {
			dist: {
				files: [{
					expand: true,
					cwd: 'app/images',
					src: '{,*/}*.{png,jpg,jpeg,gif}',
					dest: 'dist/images'
				}]
			}
		},

		svgmin: {
			dist: {
				files: [{
					expand: true,
					cwd: 'app/images',
					src: '{,*/}*.svg',
					dest: 'dist/images'
				}]
			}
		},

		htmlmin: {
			dist: {
				options: {
					collapseWhitespace: true,
					conservativeCollapse: true,
					collapseBooleanAttributes: true,
					removeCommentsFromCDATA: true
				},
				files: [{
					expand: true,
					cwd: 'dist',
					src: ['*.html'],
					dest: 'dist'
				}]
			}
		},

		ngtemplates: {
			dist: {
				options: {
					module: 'AnnotationToolApp',
					htmlmin: '<%= htmlmin.dist.options %>',
					usemin: 'scripts/scripts.js'
				},
				cwd: 'app',
				src: 'views/**/*.html',
				dest: '.tmp/templateCache.js'
			}
		},

		// ng-annotate tries to make the code safe for minification automatically
		// by using the Angular long form for dependency injection.
		ngAnnotate: {
			dist: {
				files: [{
					expand: true,
					cwd: '.tmp/concat/scripts',
					src: '*.js',
					dest: '.tmp/concat/scripts'
				}]
			}
		},

		// Copies remaining files to places other tasks can use
		copy: {
			dist: {
				files: [
				{
					expand: true,
					dot: true,
					cwd: 'app',
					dest: 'dist',
					src: [
						'*.{ico,png,txt}',
						'*.html',
						'images/{,*/}*.{webp}',
						'styles/fonts/{,*/}*.*'
					]
				}, {
					expand: true,
					cwd: '.tmp/images',
					dest: 'dist/images',
					src: ['generated/*']
				}, {
					expand: true,
					cwd: 'bower_components/font-awesome/webfonts/',
					src: '*',
					dest: 'dist/fonts/'
				},
				{
					expand: true,
					cwd: 'app',
					src: 'appConfig.js',
					dest: 'dist'
				}
				]
			},
			styles: {
				expand: true,
				cwd: 'app/styles',
				dest: '.tmp/styles/',
				src: '{,*/}*.css'
			}
		},

		// Run some tasks in parallel to speed up the build process
		concurrent: {
			server: [
				'sass'
			],
			dist: [
				'sass',
				'imagemin',
				'svgmin'
			]
		},

		// convert 'Harmony' to env
		babel: {
			options: {
				sourceMap: true,
				presets: ['env']
			},
			dist: {
				files: {
					'.tmp/concat/scripts/scripts.js': '.tmp/concat/scripts/scripts.js'
				}
			}
		},

		// scss lint
		stylelint: {
			options: {
				configFile: '.stylelintrc.json',
				formatter: 'string',
				failOnError: true
			},
			src: ['app/styles/**/*.scss']
		},

		sass: {
			options: {
				implementation: sass,
				sourceMap: true,
				precision: 10,
				includePaths: ['./bower_components'],
			},
			dist: {
				files: {
					'.tmp/styles/main.css': 'app/styles/main.scss'
				}
			}
		},

		compress: {
			main: {
				options: {
					archive: 'public/AnnotationToolApp.tar.gz'
				},
				pretty: true,
				files: [
					{
						expand: true,
						cwd: 'dist/',
						src: ['**']
					}
				]
			}
		},

		uglify: {
			options: {
				dead_code: true,
				mangle: false,
				compress: false,
				beautify: true,
				sourceMap: true
			},
			dist: {
				files: [{
					expand: true,
					src: ['.tmp/concat/scripts/scripts.js'],
					dest: '.tmp/concat/scripts/'
				}]
			}
		}
	});


	grunt.registerTask('serve', 'Compile then start a connect web server', function (target) {
		if (target === 'dist') {
			return grunt.task.run([
				'build',
				'connect:dist:keepalive'
			]);
		}
		grunt.task.run([
			'clean:server',
			'stylelint',
			'wiredep',
			'concurrent:server',
			'postcss:server',
			'connect:livereload',
			'watch'
		]);
	});

	grunt.registerTask('build', [
		'clean:dist',
		'stylelint',
		'wiredep',
		'useminPrepare',
		'concurrent:dist',
		'postcss',
		'ngtemplates',
		'concat',
		'ngAnnotate',
		'copy:dist',
		'cssmin',
		'babel',
		'uglify',
		'filerev',
		'usemin',
		'htmlmin'
	]);

	grunt.registerTask('default', [
		'newer:jshint',
		'newer:jscs',
		'build'
	]);

};
