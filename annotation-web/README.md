# Neo-AnnT

## Setup requirements

All commands are tested only on Linux (*NIX) and not on Windows.
If you are using Windows for Developing, you are on your own.
You could do development with `docker` and setup your build environment
with this. The best practice is still the following procedure.


### nvm
For best setup, use `nvm` to setup a _virtual environment_ (as known from Python)
for local development: [NVM-Repo](https://github.com/creationix/nvm/)

After this, install `node v8.11.1` with the command
	
	nvm install v8.11.1

### yarn
Select your local node installation with `nvm use v8.11.1` and install `yarn` 
"globally" for this environment with

	npm -g install yarn

Additionally, but optional, you can install the required build tools with

	npm install -g grunt-cli bower yo generator-karma generator-angular karma

## Build application
After installing `yarn`, a simple

	yarn install
	
is enough to download and install all development dependencies. (`devDependencies` in `package.json`)
 
The application is built with (if you installed `grunt` globally)

	grunt build

If you didn't install `grunt` globally, you have to run

	node_modules/.bin/grunt build
	
The built-in server is started with

	node_modules/.bin/grunt serve

## Wire with Server
Use `docker` to run the application-server.
